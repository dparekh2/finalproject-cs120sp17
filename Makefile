all: chess unittest

CXX = g++
CXXFLAGS = -Wall -Wextra -pedantic -std=c++11 -g

#The only executable (except for testing) is chess
chess: Main.o Chess.o Board.o ChessPiece.o
	$(CXX) $(CXXFLAGS) Main.o Chess.o Board.o ChessPiece.o -o chess

unittest: Unittest.o Chess.o Board.o ChessPiece.o
	$(CXX) $(CXXFLAGS) Unittest.o Chess.o Board.o ChessPiece.o -o unittest


Main.o: Chess.h Game.h Prompts.h
	$(CXX) $(CXXFLAGS) -c Main.cpp
Board.o: Game.h Prompts.h
	 $(CXX) $(CXXFLAGS) -c Board.cpp

Chess.o: Game.h Chess.h Prompts.h
	 $(CXX) $(CXXFLAGS) -c Chess.cpp Board.cpp

ChessPiece.o: Game.h Chess.h Prompts.h
	 $(CXX) $(CXXFLAGS) -c ChessPiece.cpp

Piece.o: Game.h
	 $(CXX) $(CXXFLAGS) -c Piece.cpp

Unittest.o: Chess.h Game.h Prompts.h
	$(CXX) $(CXXFLAGS) -c Unittest.cpp Board.cpp Chess.cpp ChessPiece.cpp

style:
	astyle -r './*.cpp' './*.hpp' './*.h'

clean:
	rm *.o *.orig chess unittest
