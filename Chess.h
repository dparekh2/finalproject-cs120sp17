#ifndef CHESS_H
#define CHESS_H

#include "Game.h"
#include <iostream>
#include <fstream>
#include <string>

using std::ifstream;
using std::fstream;
using std::string;

// Game status codes
// -----------------
// These enumerations are optional. You can choose to use them,
// or you can decide they're not needed. They *could* be used, for
// example, to return values from validMove() and makeMove(), and
// any other methods you want. But so long as you follow the conventions
// of those methods (>0 is success, <0 is failure), you're free to
// do things your own way.
enum status {
    LOAD_FAILURE = -10,
    SAVE_FAILURE,
    PARSE_ERROR,
    MOVE_ERROR_OUT_OF_BOUNDS,
    MOVE_ERROR_NO_PIECE,
    MOVE_ERROR_BLOCKED,
    MOVE_ERROR_CANT_CASTLE,
    MOVE_ERROR_MUST_HANDLE_CHECK,
    MOVE_ERROR_CANT_EXPOSE_CHECK,
    MOVE_ERROR_ILLEGAL,
    SUCCESS = 1,
    MOVE_CHECK,
    MOVE_CAPTURE,
    MOVE_CHECKMATE,
    MOVE_STALEMATE,
    GAME_WIN,
    GAME_OVER
};

// Possible pieces
enum PieceEnum {
    PAWN_ENUM = 0,
    ROOK_ENUM,
    KNIGHT_ENUM,
    BISHOP_ENUM,
    QUEEN_ENUM,
    KING_ENUM
};

//CLASS FOR THE "CHESSGAME" METHODS
class ChessPiece : public Piece {
 public:
  virtual ~ChessPiece() {}
 
  //METHODS FOR ALL CHESS PIECES: (specific to chess)
  //check if proposed final destination is on the board
  //checks if the proposed path is clear
  //check that the piece belongs to the correct person
  int validMove(Position start, Position end, const Board& board) const override;



  //MOVED CHECK METHOD TO CHESSGAME
  //check that the move doesn't cause check
  //  int madeCheck();

  protected:
    Player m_owner;
    int m_id;
    ChessPiece(Player owner, int id) : Piece(owner, id) {}
};


class Pawn : public ChessPiece {
protected:
    friend PieceFactory<Pawn>;
    Pawn(Player owner, int id) : ChessPiece(owner, id) {}
public:
    // This method will have piece-specific logic for checking valid moves
    // It may also call the generic Piece::validMove for common logic
    int validMove(Position start, Position end,
        const Board& board) const override;
};
class Rook : public ChessPiece {
protected:
    friend PieceFactory<Rook>;
    Rook(Player owner, int id) : ChessPiece(owner, id) {}
public:
    // This method will have piece-specific logic for checking valid moves
    // It may also call the generic Piece::validMove for common logic
    int validMove(Position start, Position end,
        const Board& board) const override;
};
class Knight : public ChessPiece {
protected:
    friend PieceFactory<Knight>;
    Knight(Player owner, int id) : ChessPiece(owner, id) {}
public:
    // This method will have piece-specific logic for checking valid moves
    // It may also call the generic Piece::validMove for common logic
    int validMove(Position start, Position end,
        const Board& board) const override;
};
class Bishop : public ChessPiece {
protected:
    friend PieceFactory<Bishop>;
    Bishop(Player owner, int id) : ChessPiece(owner, id) {}
public:
    // This method will have piece-specific logic for checking valid moves
    // It may also call the generic Piece::validMove for common logic
    int validMove(Position start, Position end,
        const Board& board) const override;
};
class Queen : public ChessPiece {
protected:
    friend PieceFactory<Queen>;
    Queen(Player owner, int id) : ChessPiece(owner , id) {}
public:
    // This method will have piece-specific logic for checking valid moves
    // It may also call the generic Piece::validMove for common logic
    int validMove(Position start, Position end,
        const Board& board) const override;
};
class King : public ChessPiece {
protected:
    friend PieceFactory<King>;
    King(Player owner, int id) : ChessPiece(owner, id) {}
public:
    // This method will have piece-specific logic for checking valid moves
    // It may also call the generic Piece::validMove for common logic
    int validMove(Position start, Position end,
        const Board& board) const override;
};

class ChessGame : public Board {
public:
    ChessGame() : Board(8, 8) {
        // Add all factories needed to create Piece subclasses
        addFactory(new PieceFactory<Pawn>(PAWN_ENUM));
        addFactory(new PieceFactory<Rook>(ROOK_ENUM));
        addFactory(new PieceFactory<Knight>(KNIGHT_ENUM));
        addFactory(new PieceFactory<Bishop>(BISHOP_ENUM));
        addFactory(new PieceFactory<Queen>(QUEEN_ENUM));
        addFactory(new PieceFactory<King>(KING_ENUM));
		//addFactory(new PieceFactory<MysteryPiece>(10));
    }

	int doMove(Position start, Position end, int isValid);
	void undoMove(Position start, Position end, Piece * captured, int isCaptured); 

    // Setup the chess board with its initial pieces
    virtual void setupBoard();
	
    // Setup the chess board with load information
    //virtual void setupLoad();
    //^^WHAT THEY HAD

    virtual int setupLoad(string loadGameName);

    
    // Whether the chess game is over
    virtual bool gameOver() const override { 
		return false; 
	}

    //METHOD THAT PRINTS THE BOARD
    void printBoard();
    
    int canParse(string command);

    //METHOD THAT SAVES THE GAME
    void save(string toSave);


    //METHOD THAT CHECKS IF THE GAME IS IN A STATE OF CHECK
    int isThereCheck();

    //METHOD THAT CHECKS IF THE GAME IS IN CHECKMATE
    //int isThereCheckMate();
    
    // Perform a move from the start Position to the end Position
    // The method returns an integer with the status
    // >= 0 is SUCCESS, < 0 is failure
    virtual int makeMove(Position start, Position end) override;
	//Are the start and end positions valid squares on the board?
	//Is there a piece at position start? If yes, continue
	//What kind of piece is this?
	//Is the type of movement valid to the piece?
	//Make the move
	//Log it

};

#endif
