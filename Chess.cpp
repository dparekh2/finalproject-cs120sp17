/*  File: Chess.cpp
    Final Project, 600.120 Spring 2017
    Name: Michelle Abt, Diva Parekh
    JHED: mabt1, dparekh2
    Section: 03
*/

#undef FOR_RELEASE

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cctype>
#include <vector>
#include "Game.h"
#include "Chess.h"
#include "Prompts.h"
#include "Terminal.h"

using std::string;
using std::cin;
using std::cout;
using std::endl;
using std::vector;
using std::ifstream;
using std::ofstream;

int ChessGame::canParse(string c1) {
    //Return 1 for parse error
    if (c1.length() != 2)
        return 1;
    if (!isalpha(c1.at(0)))
        return 1;
    if (!isdigit(c1.at(1)))
        return 1;

    return 0;
}

// Make a move on the board. Return an int, with < 0 being failure
int ChessGame::makeMove(Position start, Position end) {
    Piece * toMove = this->getPiece(start);

    if (toMove != nullptr) {
        Player startOwner = toMove->owner();

        Player turn = Board::playerTurn();

        if (turn != startOwner) {
            //Prompts::illegalMove();
            return -5;
        }
    }

    // Possibly implement chess-specific move logic here
    // We call Board's makeMove to handle any general move logic
    int isCheck = this->isThereCheck();

    int isCaptured = 0;
    int moveUndone = 0;
    int retCode = Board::makeMove(start, end);
    Piece * captured = this->getPiece(end);
    Player whosTurn = Board::playerTurn();


    //ONLY WANT TO TRY TO MAKE THE MOVE IF IT IS A VALID "BOARD MOVE"
    if (retCode > 0) {
        //starting piece
        Piece *toMove = this->getPiece(start);
        int isValid = 0;
        //only want to try to make the move if it is not nullptr
        if(toMove != nullptr) {
            int isValid = toMove->validMove(start, end, *this);

            //CHECKING TO BE SURE THAT THE MOVE COULD EVEN BE VALID
            switch(isValid) {
            case -5:
                Prompts::illegalMove();
                return isValid;
            case -8:
                Prompts::blocked();
                return isValid;
            default:
                break;
            }
        } else {
            //(toMove == nullptr)
            Prompts::noPiece();
            return -2;
        }

        //SCENARIO 3: NO ONE IS CURRENTLY IN CHECK
        if(isCheck == 0) {
            isCaptured = doMove(start, end, isValid);

            isCheck = this->isThereCheck();

            if (isCheck == 3) {
                Prompts::check(whosTurn);
                m_turn++;
            } else if (isCheck == -3) { //they put their own king in check
                Prompts::cantExposeCheck();
                undoMove(start, end, captured, isCaptured);
                moveUndone = 1;
            } else {
                //didn't start in check, not ending in check
                //do nothing
                m_turn++;
            }
        }

        //SCENARIO 1: PLAYER TRYING TO MOVE IS IN CHECK
        else if (isCheck == -3) {
            isCaptured = doMove(start, end, isValid);
            isCheck = this->isThereCheck();

            if(isCheck == 3) { //putting the other team in check
                Prompts::check(whosTurn);
                m_turn++;
            } else if(isCheck == -3) { //still in check, error, undo move
                Prompts::mustHandleCheck();
                undoMove(start, end, captured, isCaptured);
                moveUndone = 1;
            } else if(isCheck == 0) {
                m_turn++;
            }

            //SCENARIO 2: THE OTHER PLAYER'S KING IS IN CHECK
        } else if (isCheck == 3) {
            m_turn++;
            //Should never happen because this means it moved on to the next
            //player's turn while the previous player was still in check
        }

        if (isCaptured && !moveUndone) {
            Prompts::capture(whosTurn);
        }

    }

    return retCode;
}

int ChessGame::doMove(Position start, Position end, int isValid) {
    unsigned int endInd = index(end);
    unsigned int startInd = index(start);
    int capt = 0;
    if (this->getPiece(end) != nullptr) {
        Prompts::capture(Board::playerTurn());
        delete m_pieces[index(end)];
        capt = 1;
    }

    m_pieces[endInd] = this->getPiece(start);
    m_pieces[startInd] = nullptr;

    if (isValid == 11) { //pawn-queen
        delete m_pieces[index(end)];
        initPiece(4, m_pieces[endInd]->owner(), end);
        return 11;
    }

    return capt;
}

void ChessGame::undoMove(Position start, Position end, Piece * captured, int isCaptured) {
    unsigned int endInd = index(end);
    unsigned int startInd = index(start);
    Piece * toMove = this->getPiece(end);
    m_pieces[startInd] = toMove;
    m_pieces[endInd] = captured;
    if (isCaptured == 11) { //revert the queen back to pawn
        delete m_pieces[startInd];
        initPiece(1, toMove->owner(), start);
    }
}

// Setup the chess board with its initial pieces
void ChessGame::setupBoard() {
    std::vector<int> pieces {
        ROOK_ENUM, KNIGHT_ENUM, BISHOP_ENUM, QUEEN_ENUM,
        KING_ENUM, BISHOP_ENUM, KNIGHT_ENUM, ROOK_ENUM
    };
    for (size_t i = 0; i < pieces.size(); ++i) {
        initPiece(PAWN_ENUM, WHITE, Position(i, 1));
        initPiece(pieces[i], WHITE, Position(i, 0));
        initPiece(pieces[i], BLACK, Position(i, 7));
        initPiece(PAWN_ENUM, BLACK, Position(i, 6));
    }
}

//FUNCTION THAT SETS UP A "LOADED GAME"
int ChessGame::setupLoad(string loadGameName) {
    ifstream loadGameFile;
    loadGameFile.open(loadGameName);
    if (!loadGameFile.is_open()) {
        return -1;
    }

    //ACTUALLY LOAD GAME INTO BOARD
    string chessNameJunk;
    int turnsMade;
    loadGameFile >> chessNameJunk;
    loadGameFile >> turnsMade;

    //set the turn counter to one more than the "last move made"
    m_turn = turnsMade + 1;

    int ownerOfPiece;
    string boardLoc;
    Position location;
    Player whosPiece;
    int alphaCoordinate;
    int integerCoordinate;
    int pieceType;

    while (loadGameFile >> ownerOfPiece >> boardLoc >> pieceType) {

        if(ownerOfPiece == 1) {
            whosPiece = BLACK;
        }
        if(ownerOfPiece == 0) {
            whosPiece = WHITE;
        }

        alphaCoordinate = (int) (boardLoc.at(0) - 'a');
        integerCoordinate = (int) (boardLoc.at(1) - '1');

        location.x = alphaCoordinate;
        location.y = integerCoordinate;

        initPiece(pieceType, whosPiece, location);
    }

    loadGameFile.close();
    return 0;
}

//FUNCTION THAT SAVES THE CHESS GAME
void ChessGame::save(string toSave) {
    ofstream saveFile;
    saveFile.open(toSave);

    //CHECK THAT THE FILE COULE OPEN PROPERLY:
    if(saveFile.fail()) {
        Prompts::saveFailure();
        return;
    }

    //"chess" header
    saveFile << "chess" << endl;
    //add the last turn TAKEN (one less than the current turn)
    int currTurn = ChessGame::turn();
    saveFile << (currTurn -1 ) << endl;

    //Loop through whole vector (if get piece != nullptr, save the piece)
    Piece* possiblePiece;

    for(int i = 7; i >= 0; i--) {
        for(int j = 0; j < 8; j++) {
            possiblePiece = Board::getPiece(Position(j, i));
            if(possiblePiece != nullptr) {
                //PRINT 1 OR 0 DEPENDING ON OWNER
                if(possiblePiece->owner() == BLACK) {
                    saveFile << "1 ";
                } else {
                    saveFile << "0 ";
                }

                //PRINT POSITION OF THE PIECE ON THE BOARD:
                //convert the inner for loop integer [j] into an alpha character:
                char alphaCoordinate = (j + 'a');
                saveFile << alphaCoordinate;
                //print the outer for loop integer [i] + 1, then a space
                saveFile << (i+1) << " ";

                //PRINT THE ID OF THE PIECE:
                int idOfPiece = possiblePiece->id();
                saveFile << idOfPiece;
                saveFile << endl;
            }

        }
    }
    saveFile.close();
}

int ChessGame::isThereCheck() {

    Position myKing;
    Position otherKing;

    //LOOP THROUGH TO FIND THE POSITION OF BOTH OF THE KINGS
    for(unsigned int i = 0; i < m_height; i++) {
        for(unsigned int j = 0; j < m_width; j++) {
            Piece * curr = this->getPiece(Position(j, i));
            if((curr != nullptr) && (curr->id() == 5)) {
                //check to see who owns the king that was found:
                if (curr->owner() == this->playerTurn()) {
                    myKing = Position(j, i);
                } else {
                    otherKing = Position(j, i);
                }
            }
        }
    }

    //LOOP THROUGH AGAIN TO SEE IF ANY MOVE CAN "VALIDLY" MOVE TO THE OTHER USERS KING'S POSITION
    Piece* couldGetKing;
    Player ownerOfCGK;
    int validMoveToKing;
    int inCheck = 0;
    int oppInCheck = 0;

    for (unsigned int i = 0; i < m_height ; i++) {
        for(unsigned int j = 0; j < m_width; j++) {
            couldGetKing = this->getPiece(Position(j,i));
            if (couldGetKing != nullptr) {
                ownerOfCGK = couldGetKing->owner();
                if (ownerOfCGK != this->playerTurn()) {
                    validMoveToKing = couldGetKing->validMove((Position(j,i)), myKing, *this);
                    if (validMoveToKing >= 0) {
                        inCheck = 1;
                    }
                } else if (ownerOfCGK == this->playerTurn()) {
                    validMoveToKing = couldGetKing->validMove((Position(j,i)), otherKing, *this);
                    if (validMoveToKing >= 0) {
                        oppInCheck = 1;
                    }
                }
            }
        }
    }

    if(oppInCheck == 1) {
        return 3;
    } else if (inCheck == 1) {
        return -3;
    } else {
        return 0;
    }
}
/*
int ChessGame::isThereCheckMate() {
  Player whosTurn = Board::playerTurn();
  Piece* curr;
  Position currP;
  Player ownerCurr;
  Position newPos;

  int validMove;
  int stillCheck;
  int returnedByDo;

  Piece* captured; 
  for (unsigned int i = 0; i < m_height ; i++) {
        for(unsigned int j = 0; j < m_width; j++) {
            curr = this->getPiece(Position(j,i));
            currP = Position(j,i);
	    ownerCurr = curr->owner();
	    if(ownerCurr != whosTurn) {
	        for (unsigned int i = 0; i < m_height ; i++) {
		  for(unsigned int j = 0; j < m_width; j++) {
		    newPos = Position(j,i);
		    captured = this->getPiece(newPos);
		    validMove = curr->validMove(currP, newPos, *this);
		    if(validMove >= 0) {
		      returnedByDo = doMove(currP, newPos, validMove);
		      stillCheck = this->isThereCheck();
		      //check if the other team is out of check (undo move & return 0)
		      if(stillCheck != 3) {
			undoMove(curr, currP, captured, returnedByDo);
			return 0; 
		      } 
		      undoMove(curr, currP, captured, returnedByDo);
		    }
		  }
		}
	    }
	}
  }
  //must still be in check
  return 1;
  }*/



static int Alternator(int alt) {
    if (!alt) {
        Terminal::colorBg(Terminal::Color::MAGENTA);
        return 1;
    }
    Terminal::colorBg(Terminal::Color::RED);
    return 0;
}

void ChessGame::printBoard() {
    //CURRENT PIECE
    Piece* currP;
    //ID OF CURRENT PIECE
    int idOfCurr;
    //CHAR TO REPRESENT PIECE ID
    string charID;

    int alternator = 0;

    //2 nested for loops to run through each postition
    cout << "    A   B   C   D   E   F   G   H " << endl;
    for(int i = 7; i >= 0; i--) {
        cout << (i + 1) << " ";
        for(int j = 0; j < 8; j++) {
            currP = Board::getPiece(Position(j, i));
            alternator = Alternator(alternator);
            if (currP == nullptr) {
                cout << "    ";
            } else {
                idOfCurr  = currP->id();
                switch (idOfCurr) {
                case (0):
                    charID = " ♙  ";
                    break;
                case (1):
                    charID = " ♖  ";
                    break;
                case(2):
                    charID = " ♘  ";
                    break;
                case(3):
                    charID = " ♗  ";
                    break;
                case(4):
                    charID = " ♕  ";
                    break;
                case(5):
                    charID = " ♔  ";
                    break;
                default:
                    break;
                }

                if (currP->owner() == BLACK) {
                    Terminal::colorFg(0, Terminal::Color::BLACK);
                } else {
                    Terminal::colorFg(1, Terminal::Color::WHITE);
                }

                cout << charID;
            }
        }
        Terminal::set_default();
        cout << " " << (i + 1);
        cout << endl;
        alternator = !alternator;
    }
    cout << "    A   B   C   D   E   F   G   H " << endl;
}
