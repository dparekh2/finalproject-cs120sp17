#include <assert.h>
#include <cctype>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <string>
#include <cctype>


#include "Game.h"
#include "Prompts.h"

using std::string;
using std::cin;
using std::cout;
using std::endl;
using std::vector;
using std::getline;

///////////////
// Board //
///////////////

Board::~Board() {
    // Delete all pointer-based resources
    for (unsigned int i=0; i < m_width * m_height; i++)
        delete m_pieces[i];
    for (size_t i=0; i < m_registeredFactories.size(); i++)
        delete m_registeredFactories[i];
}


// Get the Piece at a specific Position, or nullptr if there is no
// Piece there or if out of bounds.
Piece* Board::getPiece(Position position) const {
    if (validPosition(position))
        return m_pieces[index(position)];
    else {
        //Prompts::outOfBounds();
        return nullptr;
    }
}


int Piece::validMove(Position start, Position end, const Board& board) const {

    //we know that both the start & end positions are on the board
    //we know that there is a piece @ the starting position (don't know if owner is correct)
    Piece * toMove = board.getPiece(start);
    Piece * moveTo = board.getPiece(end);

    Player startOwner = toMove->owner();
    if (moveTo != nullptr) {
        Player endOwner = moveTo->owner();
        if (startOwner == endOwner) {
            //Prompts::illegalMove(); //trying to move onto own piece
            return -5;
        }
    }
    /*    Player turn = board.playerTurn();
    if (turn != startOwner) {
        //Prompts::illegalMove();
        return -5;
    }*/

    return 1;
}


//General run loop
int Board::run() {
    Prompts::menu();
    char initial;
    int invalid = 1; //initializing invalid to true
    while (invalid) {
        cin >> initial;
        if (initial == '1') {
            return 1;
        } else if (initial == '2') {
            return 2;
        } else {
            Prompts::menu();
        }
    }
    return 0;
}

// Create a piece on the board using the factory.
// Returns true if the piece was successfully placed on the board
bool Board::initPiece(int id, Player owner, Position position) {
    Piece* piece = newPiece(id, owner);
    if (!piece) return false;

    // Fail if the position is out of bounds
    if (!validPosition(position)) {
        Prompts::outOfBounds();
        return false;
    }
    // Fail if the position is occupied
    if (getPiece(position)) {
        Prompts::blocked();
        return false;
    }
    m_pieces[index(position)] = piece;
    return true;
}

// Add a factory to the Board to enable producing
// a certain type of piece
bool Board::addFactory(AbstractPieceFactory* pGen) {
    // Temporary piece to get the ID
    Piece* p = pGen->newPiece(WHITE);
    int id = p->id();
    delete p;

    PieceGenMap::iterator it = m_registeredFactories.find(id);
    if (it == m_registeredFactories.end()) { // not found
        m_registeredFactories[id] = pGen;
        return true;
    } else {
        std::cout << "Id " << id << " already has a generator\n";
        return false;
    }
}

// Search the factories to find a factory that can translate `id' to
// a Piece, and use it to create the Piece. Returns nullptr if not found.
Piece* Board::newPiece(int id, Player owner) {
    PieceGenMap::iterator it = m_registeredFactories.find(id);
    if (it == m_registeredFactories.end()) { // not found
        std::cout << "Id " << id << " has no generator\n";
        return nullptr;
    } else {
        return it->second->newPiece(owner);
    }
}

int Board::makeMove(Position start, Position end) {
    //CHECK TO SEE THAT THE STARTING AND ENDING POSITIONS ARE BOTH ON THE BOARD
    if(!validPosition(start) || !validPosition(end)) {
        Prompts::outOfBounds();
        return -1;
    }
    return 1;
}
