/*  File: main.cpp
    Final Project, 600.120 Spring 2017
    Name: Michelle Abt, Diva Parekh
    JHED: mabt1, dparekh2
    Section: 03
*/

#undef FOR_RELEASE

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cctype>
#include <vector>
#include "Game.h"
#include "Chess.h"
#include "Prompts.h"

using std::string;
using std::cin;
using std::cout;
using std::endl;
using std::vector;
using std::ifstream;
using std::getline;

int main() {
    ChessGame chess = ChessGame();
    int initialAction = chess.run(); //whether to load or start new
    if (initialAction == 1) {
        chess.setupBoard();
        chess.m_turn = 1;
    } else {
        Prompts::loadGame();
        string toOpen;
        cin >> toOpen;

        int loadSuccess = chess.setupLoad(toOpen);

        if(loadSuccess == -1) {
            Prompts::loadFailure();
            return 1;
        }
    }

    string c1;
    string c2;
    string toSave;
    int boardToggle = 0;
    // chess.m_turn = 1;
    int gameOver = 0;
    Position start;
    Position end;
    //it should be while (!chess.gameOver())

    while (!gameOver) {

        Prompts::playerPrompt(chess.playerTurn(), chess.turn());

        cin >> c1;

        for (unsigned int i = 0; i < c1.length(); i++) {
            c1.at(i) = tolower(c1.at(i));
        }

        if (!c1.compare("q")) {
            gameOver = 1;
            return 1;
        }

        if (!c1.compare("save")) {
            Prompts::saveGame();
            //NAME OF THE OUTFILE
            cin >> toSave;
            //save the thing
            chess.save(toSave);
            continue;
        }

        if (!c1.compare("board")) {
            boardToggle = !boardToggle;
            continue;
        }

        if (!c1.compare("forfeit")) {
            chess.m_turn++;
            Player wins = chess.playerTurn();
            chess.m_turn--;
            Prompts::win(wins, chess.turn());
            Prompts::gameOver();
            gameOver = 1;
            return 1;
        }

        if (chess.canParse(c1) == 1) {
            Prompts::parseError();
        } else {
            cin >> c2;
            for (unsigned int i = 0; i < c2.length(); i++) {
                c2.at(i) = tolower(c2.at(i));
            }
            if (chess.canParse(c2) == 1) {
                Prompts::parseError();
            } else {
                int sCol = (int) (c1.at(0) - 'a');
                int eCol = (int) (c2.at(0) - 'a');
                int sRow = (int) c1.at(1) - '1';
                int eRow = (int) c2.at(1) - '1';
                start.x = sCol;
                start.y = sRow;
                end.x = eCol;
                end.y = eRow;
                chess.makeMove(start, end);
            }

            if (boardToggle) {
                chess.printBoard();
            }
        }
    }
    return 0;

}
