/*  File: Unittest.cpp
    Final Project, 600.120 Spring 2017
    Name: Michelle Abt, Diva Parekh
    JHED: mabt1, dparekh2
    Section: 03
*/

#include <cassert>
#include <iostream>
#include <string>
#include <cstdlib>
#include <fstream>

#include "Game.h"
#include "Chess.h"
#include "Prompts.h"


using std::endl;
using std::cout;

void loadFails();
void loadSucceeds();
void pawnValid();
void pawnInvalid();
void knightValid();
void knightInvalid();
void rookValid();
void rookInvalid();
void check();
void checkmate();
void stalemate();

int main() {
    loadFails();
    loadSucceeds();
    pawnValid();
    pawnInvalid();
    knightValid();
    knightInvalid();
    rookValid();
    rookInvalid();
    check();
    checkmate();
    stalemate();
    cout<< "All Tests Passed!" << endl;
    return 0;
}

void loadFails() {
    ChessGame chess = ChessGame();
    assert(chess.setupLoad("Nothing.txt") == -1);
}

void loadSucceeds() {
    ChessGame chess = ChessGame();
    assert(chess.setupLoad("aboutToQueen.txt") == 0);
}

void pawnValid() {
    ChessGame chess = ChessGame();
    chess.setupBoard();
    Position start = Position(4, 1);
    Position end = Position(4, 3);
    Piece * pawn = chess.getPiece(start);
    int toTest = pawn->validMove(start, end, chess);
    assert(toTest == 1);
    start = Position(7, 1);
    end = Position (7, 2);
    pawn = chess.getPiece(start);
    toTest = 0;
    toTest = pawn->validMove(start, end, chess);
    assert(toTest == 1);
}

void pawnInvalid() {
    ChessGame chess = ChessGame();
    chess.setupBoard();
    Position start = Position(4, 1);
    Position end = Position(4, 6);
    Piece * pawn = chess.getPiece(start);
    int toTest = pawn->validMove(start, end, chess);
    assert(toTest == -5);
    end = Position(5, 2);
    toTest = pawn->validMove(start, end, chess);
    assert(toTest == -5);
    end = Position(5, 1);
    toTest = pawn->validMove(start, end, chess);
    assert(toTest == -5);
}

void knightValid() {
    ChessGame chess = ChessGame();
    Position start = Position(4, 4);
    chess.initPiece(2, WHITE, start);
    Position end = Position(5, 6);
    Piece * knight = chess.getPiece(start);
    int toTest = knight->validMove(start, end, chess);
    assert (toTest == 1);
    end = Position(6, 5);
    toTest = knight->validMove(start, end, chess);
    end = Position(2, 3);
    toTest = knight->validMove(start, end, chess);
    assert (toTest == 1);
    end = Position(3, 2);
    toTest = knight->validMove(start, end, chess);
    assert (toTest == 1);
    end = Position(2, 5);
    toTest = knight->validMove(start, end, chess);
    assert (toTest == 1);
    end = Position(3, 6);
    toTest = knight->validMove(start, end, chess);
    assert (toTest == 1);
}

void knightInvalid() {
    ChessGame chess = ChessGame();
    Position start = Position(4, 4);
    chess.initPiece(2, WHITE, start);
    Position end = Position(5, 7);
    Piece * knight = chess.getPiece(start);
    int toTest = knight->validMove(start, end, chess);
    assert (toTest == -5);
}

void rookValid() {
    ChessGame chess = ChessGame();
    Position start = Position(4, 4);
    chess.initPiece(1, WHITE, start);
    Position end = Position(7, 4);
    Piece * rook = chess.getPiece(start);
    int toTest = rook->validMove(start, end, chess);
    assert (toTest == 1);
}

void rookInvalid() {
    ChessGame chess = ChessGame();
    Position start = Position(4, 4);
    chess.initPiece(1, WHITE, start);
    chess.initPiece(2, WHITE, Position(5, 4));
    Position end = Position(7, 4);
    Piece * rook = chess.getPiece(start);
    int toTest = rook->validMove(start, end, chess);
    assert (toTest == -8);
}

void check() {
    ChessGame chess = ChessGame();
    chess.setupLoad("check.txt");
    int check = chess.isThereCheck();
    assert(check == -3);
}

void checkmate() {

}

void stalemate() {

}
