#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cctype>
#include <vector>
#include "Game.h"
#include "Chess.h"
#include "Prompts.h"
#include "Terminal.h"

using std::string;
using std::cin;
using std::cout;
using std::endl;
using std::vector;
using std::ifstream;


int ChessPiece::validMove(Position start, Position end, const Board& board) const {
    int genValid = this->Piece::validMove(start, end, board);
    if (genValid >= 0) {
        int startX = start.x;
        int startY = start.y;
        int endX = end.x;
        int endY = end.y;

        if ((startX == endX) && (startY == endY)) {
            //Prompts::illegalMove();
            return -5;
        } //if you're trying to move it to the same spot
    }

    return genValid;
    //DEAL WITH CHECKED HERE, BUT NOT BLOCKED MOVE
    //TOO COMPLICATED TO DO GENERICALLY
    //Deal with checked once valid methods for all other pieces are done
}


int Pawn::validMove(Position start, Position end, const Board& board) const {
    int chessValid = this->ChessPiece::validMove(start, end, board);
    if (chessValid >= 0) {
        int startX = start.x;
        int startY = start.y;
        int endX = end.x;
        int endY = end.y;
        Piece * toMove = board.getPiece(start);
        Player startOwner = toMove->owner();

        //If a pawn is at its starting position, it can move two spots
        //Have to check owner bc pawn can only move forward
        //Have to check if capture because only then can pawn move diagonally
        int diffX = abs(startX - endX);
        int diffY = abs(startY - endY);

        //Can't move backwards
        if (startOwner == WHITE) {
            if (endY < startY) {
                //Prompts::illegalMove();
                return -5;
            }
        } else {
            if (startY < endY) {
                //Prompts::illegalMove();
                return -5;
            }
        }

        Piece * temp;


        if ((diffX == 0) && (diffY == 2)) { //trying to move two spots
            //make sure if it's trying to do this it should only be moving
            //straight two spots and it should be at the start

            if (startX != endX) {
                //Prompts::illegalMove();
                return -5;
            } //makes sure it's moving only vertically if it wants to move two spots


            //Checks that it has not moved yet
            if (startOwner == WHITE) {
                if (startY != 1) {
                    //Prompts::illegalMove();
                    return -5;
                }
            } else {
                if (startY != 6) {
                    //Prompts::illegalMove();
                    return -5;
                }
            }

            //Checks if path is blocked
            if (startY < endY) {
                int interimY = startY + 1;
                temp = board.getPiece(Position(startX, interimY));
                if (temp != nullptr) {
                    //Prompts::blocked();
                    return -8;
                }
            } else {
                int interimY = startY - 1;
                temp = board.getPiece(Position(startX, interimY));
                if (temp != nullptr) {
                    //Prompts::blocked();
                    return -8;
                }
            }

        } else if (((diffX == 1) && (diffY == 1)) || ((diffX == 0) && (diffY == 1)) ) { //only trying to move one spot
            temp = board.getPiece(Position(endX, endY));

            if (diffX == 1) { //if it moves diagonally, there needs to be a capture
                if (temp == nullptr) {
                    //Prompts::illegalMove();
                    return -5;
                }
            }

            //If it moves in a straight line, there cannot be a capture
            if (diffX == 0) {
                if (temp != nullptr) {
                    return -5;
                }
            }

            //we know it's trying to move one spot and straight vertically
            //now check to make sure there's no opposing team piece on end
            if (startY == endY) {
                if (temp != nullptr) {
                    //Prompts::blocked();
                    return -8;
                }
            }
        } else {
            //Prompts::illegalMove();
            return -5;
        }
        if ((endY == 7) || (endY == 0)) {
            return 11;
        }
        return 1;
    } else {
        return chessValid;
    }
}

int Bishop::validMove(Position start, Position end, const Board& board) const {
    int chessValid = this->ChessPiece::validMove(start, end, board);
    if (chessValid >= 0) {
        int startX = start.x;
        int startY = start.y;
        int endX = end.x;
        int endY = end.y;
        Piece * temp;
        Position pos = Position(startX, startY);
        int diffX = abs(startX - endX);
        int diffY = abs(startY - endY);
        if (diffX != diffY) {
            //Prompts::illegalMove();
            return -5;
        }

        if ((startX < endX) && (startY < endY)) {
            int j = startY + 1;
            for (int i = startX + 1; i < endX; i++) {
                pos = Position(i, j);
                temp = board.getPiece(pos);
                if (temp != nullptr) {
                    //Prompts::blocked();
                    return -8;
                }
                j++;
            }
        } //up right

        if ((startX < endX) && (startY > endY)) {
            int j = startY - 1;
            for (int i = startX + 1; i < endX; i++) {
                pos = Position(i, j);
                temp = board.getPiece(pos);
                if (temp != nullptr) {
                    //Prompts::blocked();
                    return -8;
                }
                j--;
            }
        } //down right

        if ((startX > endX) && (startY > endY)) {
            int j = startY - 1;
            for (int i = startX - 1; i > startX; i--) {
                pos = Position(i, j);
                temp = board.getPiece(pos);
                if (temp != nullptr) {
                    //Prompts::blocked();
                    return -8;
                }
                j--;
            }
        } //down left

        if ((startX > endX) && (startY < endY)) {
            int j = startY + 1;
            for (int i = startX - 1; i > endX; i--) {
                pos = Position(i, j);
                temp = board.getPiece(pos);
                if (temp != nullptr) {
                    //Prompts::blocked();
                    return -8;
                }
                j++;
            }
        } //up left

        return 1;
    } else {
        return chessValid;
    }
}

int Knight::validMove(Position start, Position end, const Board& board) const {
    int chessValid = this->ChessPiece::validMove(start, end, board);
    if (chessValid >= 0) {
        int startX = start.x;
        int startY = start.y;
        int endX = end.x;
        int endY = end.y;
        int diffX = abs(startX - endX);
        int diffY = abs(startY - endY);

        if (!(diffX == 2 && diffY == 1) && !(diffX == 1 && diffY == 2)) {
            //Prompts::illegalMove();
            return -5;
        }
        return 1;
    } else {
        return chessValid;
    }
}

int Rook::validMove(Position start, Position end, const Board& board) const {
    int chessValid = this->ChessPiece::validMove(start, end, board);
    if (chessValid >= 0) {
        int startX = start.x;
        int startY = start.y;
        int endX = end.x;
        int endY = end.y;
        Piece * temp;
        Position pos = Position(startX, startY);
        if (startX == endX) {
            if (startY < endY) {
                for (int i = startY + 1; i < endY; i++) {
                    pos = Position(startX, i);
                    temp = board.getPiece(pos);
                    if (temp != nullptr) {
                        //Prompts::blocked();
                        return -8;
                    }
                }
            } else {
                for (int i = endY - 1; i > startY; i--) {
                    pos = Position(startX, i);
                    temp = board.getPiece(pos);
                    if (temp != nullptr) {
                        //Prompts::blocked();
                        return -8;
                    }
                }
            }
        } else if (startY == endY) {
            if (startX < endX) {
                for (int i = startX + 1; i < endX; i++) {
                    pos = Position(i, startY);
                    temp = board.getPiece(pos);
                    if (temp != nullptr) {
                        //Prompts::blocked();
                        return -8;
                    }
                }
            } else {
                for (int i = endX - 1; i > startX; i--) {
                    pos = Position(i, startY);
                    temp = board.getPiece(pos);
                    if (temp != nullptr) {
                        //Prompts::blocked();
                        return -8;
                    }
                }
            }
        } else {
            //not a straight line
            //Prompts::illegalMove();
            return -5;
        }
        return 1;
    } else {
        return chessValid;
    }
}

int Queen::validMove(Position start, Position end, const Board& board) const {
    int chessValid = this->ChessPiece::validMove(start, end, board);
    if (chessValid >= 0) {
        int startX = start.x;
        int startY = start.y;
        int endX = end.x;
        int endY = end.y;
        Piece * temp;
        Position pos = Position(startX, startY);
        int diffX = abs(startX - endX);
        int diffY = abs(startY - endY);
        if ((diffX != diffY) && !((startX == endX) || (startY == endY))) {
            //Prompts::illegalMove();
            return -5;
        } //checks that movement is either diagonal or in a straight line

        if (diffX == diffY) {
            if ((startX < endX) && (startY < endY)) {
                int j = startY + 1;
                for (int i = startX + 1; i < endX; i++) {
                    pos = Position(i, j);
                    temp = board.getPiece(pos);
                    if (temp != nullptr) {
                        //Prompts::blocked();
                        return -8;
                    }
                    j++;
                }
            } //up right

            if ((startX < endX) && (startY > endY)) {
                int j = startY - 1;
                for (int i = startX + 1; i < endX; i++) {
                    pos = Position(i, j);
                    temp = board.getPiece(pos);
                    if (temp != nullptr) {
                        //Prompts::blocked();
                        return -8;
                    }
                    j--;
                }
            } //down right

            if ((startX > endX) && (startY > endY)) {
                int j = startY - 1;
                for (int i = startX - 1; i > startX; i--) {
                    pos = Position(i, j);
                    temp = board.getPiece(pos);
                    if (temp != nullptr) {
                        //Prompts::blocked();
                        return -8;
                    }
                    j--;
                }
            } //down left

            if ((startX > endX) && (startY < endY)) {
                int j = startY + 1;
                for (int i = startX - 1; i > endX; i--) {
                    pos = Position(i, j);
                    temp = board.getPiece(pos);
                    if (temp != nullptr) {
                        //Prompts::blocked();
                        return -8;
                    }
                    j++;
                }
            } //up left

        }

        else if (startX == endX) {
            if (startY < endY) {
                for (int i = startY + 1; i < endY; i++) {
                    pos = Position(startX, i);
                    temp = board.getPiece(pos);
                    if (temp != nullptr) {
                        //Prompts::blocked();
                        return -8;
                    }
                }
            } else {
                for (int i = endY - 1; i > startY; i--) {
                    pos = Position(startX, i);
                    temp = board.getPiece(pos);
                    if (temp != nullptr) {
                        //Prompts::blocked();
                        return -8;
                    }
                }
            }
        } else if (startY == endY) {
            if (startX < endX) {
                for (int i = startX + 1; i < endX; i++) {
                    pos = Position(i, startY);
                    temp = board.getPiece(pos);
                    if (temp != nullptr) {
                        //Prompts::blocked();
                        return -8;
                    }
                }
            } else {
                for (int i = endX - 1; i > startX; i--) {
                    pos = Position(i, startY);
                    temp = board.getPiece(pos);
                    if (temp != nullptr) {
                        //Prompts::blocked();
                        return -8;
                    }
                }
            }
        }

        return 1;
    } else {
        return chessValid;
    }
}

int King::validMove(Position start, Position end, const Board& board) const {
    int chessValid = this->ChessPiece::validMove(start, end, board);
    int startX = start.x;
    int startY = start.y;
    int endX = end.x;
    int endY = end.y;
    if (chessValid >= 0) {
        int diffX = abs(startX - endX);
        int diffY = abs(startY - endY);
        if ((diffX > 1) || (diffY > 1)) {
            //Prompts::illegalMove();
            return -5;
        }
        return 1;
    } else {
        return chessValid;
    }
}
